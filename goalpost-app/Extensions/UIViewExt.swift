//
//  UIViewExt.swift
//  goalpost-app
//
//  Created by Igor Chernyshov on 18.12.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import UIKit

extension UIView {
    func bindToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil) // Listen to a default notification which is fired every time the keyboard opens
    }
    
    @objc func keyboardWillChange(_ notification: NSNotification) { // "_ " means that this parameter is optional
        // Capture which animation type and speed the user has
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let startingFrame = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue // Gets a coordinate of a point where the keyboard starts it animation and force unwrap it as a cgRectValue for future use
        let endingFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue // Same for the ending frame
        let deltaY = endingFrame.origin.y - startingFrame.origin.y
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY // Add the keyboard height to the UIView
        }, completion: nil)
    }
}
