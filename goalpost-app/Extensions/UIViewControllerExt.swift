//
//  UIViewControllerExt.swift
//  goalpost-app
//
//  Created by Igor Chernyshov on 18.12.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush // Pushes old content with a new content
        transition.subtype = kCATransitionFromRight // Sets the direction to push. Here - from the right to the left
        self.view.window?.layer.add(transition, forKey: kCATransition) // "Self" means "for whatever VC we take". kCATransition is a key that is there by default
        
        present(viewControllerToPresent, animated: false, completion: nil) // Animeted: false because we've already overridden it. In case of true it'd call a default animation from bottom to top
    }
    
    func presentSecondaryDetail(_ viewControllerToPresent: UIViewController) { // Function that dismisses the old VC and present a new one instead
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        guard let presentedViewController = presentedViewController else { return }
        
        presentedViewController.dismiss(animated: false) {
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.present(viewControllerToPresent, animated: false, completion: nil)
        }
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
}
