//
//  GoalCell.swift
//  goalpost-app
//
//  Created by Igor Chernyshov on 11.12.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import UIKit

class GoalCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var goalDescriptionLabel: UILabel!
    @IBOutlet weak var goalTypeLabel: UILabel!
    @IBOutlet weak var goalProgressLabel: UILabel!
    @IBOutlet weak var completionView: UIView!
    
    func configureCell(goal: Goal) {
        self.goalDescriptionLabel.text = goal.goalDescription
        self.goalTypeLabel.text = goal.goalType
        self.goalProgressLabel.text = String(describing: goal.goalProgress)
        
        if goal.goalCompletionValue == goal.goalProgress {
            self.completionView.isHidden = false
        } else {
            self.completionView.isHidden = true
        }
    }
}
