//
//  FinishGoalVC.swift
//  goalpost-app
//
//  Created by Igor Chernyshov on 19.12.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import UIKit
import CoreData

class FinishGoalVC: UIViewController, UITextFieldDelegate {
    
    // Outlets
    @IBOutlet weak var createGoalButton: UIButton!
    @IBOutlet weak var pointsText: UITextField!
    
    var goalDescription: String!
    var goalType: GoalType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pointsText.delegate = self
        createGoalButton.bindToKeyboard()
        
    }
    
    func initData(description: String, type: GoalType) {
        self.goalDescription = description
        self.goalType = type
    }
    
    @IBAction func createGoalButtonPressed(_ sender: Any) {
        if pointsText.text != "" && pointsText.text != "0" {
            self.save { (complete) in
                if complete {
                    dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismissDetail()
    }
    
    func save(completion: (_ finished: Bool) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return } // This is how we get our managed object context from the main queue
        let goal = Goal(context: managedContext) // goal now equals Goal entity from a core data
        
        // Setup a model here
        goal.goalDescription = goalDescription
        goal.goalType = goalType.rawValue
        goal.goalCompletionValue = Int32(pointsText.text!)!
        goal.goalProgress = Int32(0)
        
        // Try to save here
        do {    // DO the following
            try managedContext.save() // TRY to save
            print("Successfuly saved data.")
            completion(true) // We are good
        } catch {   // In case of errors
            debugPrint("Could not save: \(error.localizedDescription).") // Print a debug
            completion(false) // We are not good
        }
    }
}
