//
//  GoalType.swift
//  goalpost-app
//
//  Created by Igor Chernyshov on 17.12.2017.
//  Copyright © 2017 Igor Chernyshov. All rights reserved.
//

import Foundation

enum GoalType: String {
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}
